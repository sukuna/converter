
SHELL = /bin/bash

SRC_DIR ?= src
TEST ?= .
LINE_LENGTH ?= 120
MYPY_VERSION=0.761
MAX_COMPLEXITY ?= 10
MIN_TEST_COVERAGE ?= 100

DELETE_PYCACHE = find . -follow -type f -name '*.py[co]' -delete && find . -follow -type d -name __pycache__ -delete
LOG_FORMAT = "%(t)s %(l)s %(h)s %(l)s \"%(r)s\" %(s)s %(b)s \"%(f)s\" \"%(a)s"

.PHONY: run

all: build format mypy flake8 test safety

run: build format_check safety
	@cd src && \
	poetry run gunicorn --bind="0.0.0.0:8000" --access-logfile="-" --access-logformat=${LOG_FORMAT} --reload main:api

clean:
	rm -rf .venv requirements.txt
	rm -rf dist
	$(DELETE_PYCACHE)


build: env

env:
	poetry config virtualenvs.in-project true
	poetry install || (rm -rf .venv && exit -1)
	poetry export --without-hashes --format=requirements.txt > requirements.txt

test:
	rm -f .coverage
	PYTHONPATH="$(SRC_DIR):$(PYTHONPATH)" \
	poetry run py.test \
	    --cov=$(SRC_DIR) \
	    --cov-branch \
		--cov-report term-missing \
	    --cov-fail-under=$(MIN_TEST_COVERAGE) \
	    --log-level=INFO \
	    --showlocals \
	    --verbose \
	    $(abspath $(TEST)); \
	EXIT_CODE=$$?; \
	if [ "$$EXIT_CODE" -eq "0" ] || [ "$$EXIT_CODE" -eq "5" ]; then exit 0; else exit $$EXIT_CODE; fi

format:
	if [ -d .venv ]; then VENV="--virtual-env=.venv"; else VENV=""; fi; \
	poetry run isort $$VENV --line-width=$(LINE_LENGTH) --multi-line=3 --trailing-comma --atomic --recursive .
	poetry run black --line-length=$(LINE_LENGTH) .

format_check:
	if [ -d .venv ]; then VENV="--virtual-env=.venv"; else VENV=""; fi ;\
	poetry run isort $$VENV --line-width=$(LINE_LENGTH) --multi-line=3 --trailing-comma --check-only --diff --stdout --recursive .
	poetry run black --check --diff --line-length=$(LINE_LENGTH) .

flake8:
	@# E203 issue: https://github.com/PyCQA/pycodestyle/issues/373
	poetry run flake8 --exclude=.venv --extend-ignore=E203 --max-complexity=$(MAX_COMPLEXITY) --max-line-length=$(LINE_LENGTH) .

mypy:
	if [ -d tests ]; then TESTS=tests; else TESTS=""; fi; \
	if [ -f $(SRC_DIR)/mypy.ini ]; then MYPY_CONFIG="--config-file=$(SRC_DIR)/mypy.ini"; else MYPY_CONFIG=""; fi; \
	MYPYPATH="/stubs:$(SRC_DIR):$(SRC_DIR)/stubs" poetry run mypy --allow-untyped-decorators --pretty --strict $$MYPY_CONFIG $(SRC_DIR) $$TESTS


safety:
	poetry run safety check
	poetry run bandit --exclude /tests/ --recursive $(SRC_DIR)
