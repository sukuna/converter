# Currency Converter Excercise

Currency converter excercise

## Getting Started

The excercise run on Linux (MacOS non tested).

### Prerequisites

```
Docker
```


## Running the tests

Use dev mode to run test

```
./run.sh dev
```

and then run command inside shell

```
make
```


## Running project


### From run script in run mode

```
./run.sh run
```

### From dev mode

```
./run.sh dev
make run
```

## Test endpoint

Open url [http://localhost:8000/convert](http://localhost:8000/convert) in a browser

### Example query

[http://localhost:8000/convert?amount=20&src_currency=EUR&dest_currency=EUR&reference_date=2020-02-04](http://localhost:8000/convert?amount=20&src_currency=EUR&dest_currency=EUR&reference_date=2020-02-04)