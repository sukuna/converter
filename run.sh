#!/usr/bin/env sh

DOCKER_TAG="execise"
DOCKER_ARGS="--rm -ti -v ${PWD}:/app -w /app -p 8000:8000"

DOCKER_BUILD="docker build -t ${DOCKER_TAG} ."
DOCKER_SHELL="docker run --rm ${DOCKER_ARGS} --entrypoint bash ${DOCKER_TAG}"
DOCKER_RUN="docker run --rm ${DOCKER_ARGS} ${DOCKER_TAG} run"

if [ $# -lt 1 ]
then
    echo "Usage : $0 run to start application exchange rates"
    echo "Usage : $0 shell for interactive mode"
    exit
fi


case "$1" in

    dev)  echo "Dev mode"
            ${DOCKER_BUILD} && ${DOCKER_SHELL}
    ;;

    run)    echo "Run mode" 
            ${DOCKER_BUILD} && ${DOCKER_RUN}
    ;;

    *) echo "Invalid option"
    ;;

esac
    