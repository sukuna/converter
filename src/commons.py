from typing import Dict, Optional

RATE_TYPE = Optional[float]
CURRENCY_RATE_ROW_TYPE = Dict[str, RATE_TYPE]
EXCHANGE_RATES_TYPE = Dict[str, CURRENCY_RATE_ROW_TYPE]
