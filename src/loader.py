from collections import defaultdict
from typing import Optional

import requests
import xmltodict

from commons import EXCHANGE_RATES_TYPE, RATE_TYPE

EXCHANGE_RATES_URL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"


def download(url: str) -> Optional[str]:
    response = requests.get(url, timeout=30)
    if response.status_code == requests.codes.ok:
        return response.text
    else:
        # response.raise_for_status()
        return None


def convert_to_float(value: str) -> RATE_TYPE:
    try:
        return float(value)
    except Exception:
        return None


def convert_to_dict(content: str) -> EXCHANGE_RATES_TYPE:
    data = xmltodict.parse(content)
    to_dict = dict(
        (cube["@time"], dict((rate["@currency"], convert_to_float(rate["@rate"])) for rate in cube["Cube"]))
        for cube in data["gesmes:Envelope"]["Cube"]["Cube"]
    )

    return to_dict


def load_exchange_rates() -> EXCHANGE_RATES_TYPE:
    try:

        content = download(EXCHANGE_RATES_URL)
        if content:
            data = convert_to_dict(content)
            return defaultdict(None, **data)
        else:
            print(f"Error loading exchange rate file content {content}")
            return defaultdict(dict)

    except Exception as e:
        print(f"Error loading exchange rate file {e}")
        return defaultdict(dict)
