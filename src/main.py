import datetime

import falcon
import voluptuous
from voluptuous import All, Coerce, Length, Range, Required, Schema

import loader
from commons import CURRENCY_RATE_ROW_TYPE, EXCHANGE_RATES_TYPE, RATE_TYPE

DEFAULT_CURRENCY = "EUR"


def Timestamp(value: str) -> datetime.datetime:
    return datetime.datetime.strptime(value, "%Y-%m-%d")


def get_currency_rate(currencies: CURRENCY_RATE_ROW_TYPE, currency: str) -> RATE_TYPE:
    if currency == DEFAULT_CURRENCY:
        return 1

    rate = currencies.get(currency)

    if rate is not None:
        return rate
    else:
        raise ValueError(f"Currency {currency} not found")


class ExchangeRatesResource:
    def __init__(self, exchange_rates: EXCHANGE_RATES_TYPE) -> None:
        self.exechange_rates = exchange_rates
        self.validation_schema = Schema(
            {
                Required("amount"): All(Coerce(float), Range(min=1.0)),
                Required("src_currency"): All(str, Length(min=3, max=3)),
                Required("dest_currency"): All(str, Length(min=3, max=3)),
                Required("reference_date"): Timestamp,
            }
        )

    def on_get(self, req: falcon.Response, resp: falcon.Request) -> None:
        try:
            self.validation_schema(req.params)

            currencies = self.exechange_rates.get(req.params["reference_date"])

            if not currencies:
                raise ValueError(f"Date {req.params['reference_date']} not found ")

            src_exchange_rate = get_currency_rate(currencies, req.params["src_currency"])
            dest_exchange_rate = get_currency_rate(currencies, req.params["dest_currency"])

            amount = float(req.params["amount"]) / src_exchange_rate * dest_exchange_rate  # type: ignore

            quote = {
                "amount": round(amount, 4),
                "currency": req.params["dest_currency"],
            }

            resp.media = quote
            resp.status = falcon.HTTP_200

        except voluptuous.error.MultipleInvalid as e:
            resp.media = {"error": str(e)}
            resp.status = falcon.HTTP_400
        except ValueError as e:  # pragma: no cover
            resp.media = {"error": str(e)}
            resp.status = falcon.HTTP_400
        except Exception as e:  # pragma: no cover
            resp.media = {"error": "Internal server error"}
            resp.status = falcon.HTTP_500
            print(f"Internal server error {e}")


def create_app(exchange_rates: EXCHANGE_RATES_TYPE) -> falcon.API:
    exchange_rates_resource = ExchangeRatesResource(exchange_rates)
    api = falcon.API()
    api.add_route("/convert", exchange_rates_resource)
    return api


def get_app() -> falcon.API:
    exchange_rates = loader.load_exchange_rates()
    return create_app(exchange_rates)


api = get_app()
