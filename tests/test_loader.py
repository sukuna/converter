from unittest.mock import MagicMock, patch

import requests

import loader


def test_download() -> None:

    with patch.object(requests, "get", return_value=MagicMock()) as mock_req:
        mock_req.return_value.status_code = requests.codes.ok
        mock_req.return_value.text = "<text>test</text>"
        content = loader.download("http://fake")
        assert content == "<text>test</text>"

    with patch.object(requests, "get", return_value=MagicMock()) as mock_req:
        mock_req.return_value.status_code = requests.codes.timeout
        mock_req.return_value.text = "<text>test</text>"
        content = loader.download("http://fake")
        assert content is None


def test_convert_to_dict() -> None:
    body = """
    <gesmes:Envelope>
        <Cube>
            <Cube time="2020-01-31">
                <Cube currency="USD" rate="1.1052"/>
                <Cube currency="EUR" rate="1.1052"/>
            </Cube>
            <Cube time="2020-01-30">
                <Cube currency="EUR" rate="1.1052"/>
                <Cube currency="USD" rate="1.1052"/>
            </Cube>
        </Cube>
    </gesmes:Envelope>
    """
    response = loader.convert_to_dict(body)

    assert response == {"2020-01-31": {"USD": 1.1052, "EUR": 1.1052}, "2020-01-30": {"EUR": 1.1052, "USD": 1.1052}}


def test_convert_to_dict_with_no_float() -> None:
    body = """
    <gesmes:Envelope>
        <Cube>
            <Cube time="2020-01-31">
                <Cube currency="USD" rate="1.1052"/>
                <Cube currency="EUR" rate="None"/>
            </Cube>
            <Cube time="2020-01-30">
                <Cube currency="EUR" rate="None"/>
                <Cube currency="USD" rate="1.1052"/>
            </Cube>
        </Cube>
    </gesmes:Envelope>
    """
    response = loader.convert_to_dict(body)
    assert response == {"2020-01-31": {"USD": 1.1052, "EUR": None}, "2020-01-30": {"EUR": None, "USD": 1.1052}}


def test_load_exchange_rates_empty() -> None:

    with patch.object(requests, "get", return_value=MagicMock()) as mock_req:
        mock_req.return_value.status_code = requests.codes.ok
        mock_req.return_value.text = ""

        exchange_rates = loader.load_exchange_rates()
        assert not exchange_rates


def test_load_exchange_rates_loadded() -> None:
    body = """
    <gesmes:Envelope>
        <Cube>
            <Cube time="2020-01-31">
                <Cube currency="USD" rate="1.1052"/>
                <Cube currency="EUR" rate="None"/>
            </Cube>
            <Cube time="2020-01-30">
                <Cube currency="EUR" rate="None"/>
                <Cube currency="USD" rate="1.1052"/>
            </Cube>
        </Cube>
    </gesmes:Envelope>
    """
    with patch.object(requests, "get", return_value=MagicMock()) as mock_req:
        mock_req.return_value.status_code = requests.codes.ok
        mock_req.return_value.text = body

        exchange_rates = loader.load_exchange_rates()
        assert len(exchange_rates.keys()) == 2


def test_load_exchange_rates_excpetion() -> None:
    with patch.object(requests, "get", return_value=Exception):
        exchange_rates = loader.load_exchange_rates()
        assert not exchange_rates
