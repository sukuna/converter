from typing import Generator
from unittest.mock import MagicMock, patch

import falcon
import pytest
import requests
from falcon import testing

import main


@pytest.fixture()
def client(data: str) -> testing.TestClient:
    return testing.TestClient(main.get_app())


@pytest.fixture()
def data() -> Generator[str, None, None]:
    body = """
    <gesmes:Envelope>
        <Cube>
            <Cube time="2020-02-04">
                <Cube currency="USD" rate="1.1052"/>
                <Cube currency="EUR" rate="None"/>
            </Cube>
            <Cube time="2020-02-03">
                <Cube currency="USD" rate="1.1052"/>
                <Cube currency="EUR" rate="None"/>
            </Cube>
            <Cube time="2020-02-02">
                <Cube currency="EUR" rate="None"/>
                <Cube currency="USD" rate="1.1052"/>
            </Cube>
        </Cube>
    </gesmes:Envelope>
    """
    with patch.object(requests, "get", return_value=MagicMock()) as mock_req:
        mock_req.return_value.status_code = requests.codes.ok
        mock_req.return_value.text = body
        yield mock_req


def test_get_exchange_params_errors(client: testing.TestClient) -> None:
    result = client.simulate_get(path="/convert")
    assert result.json["error"].startswith("required key not provided @ data")
    assert result.status == falcon.HTTP_400

    result = client.simulate_get(
        path="/convert",
        params={"amount": "A", "src_currency": "EUR", "dest_currency": "USD", "reference_date": "2019-12-31"},
    )
    assert result.json["error"].startswith("expected float for dictionary value @ data['amount']")
    assert result.status == falcon.HTTP_400

    result = client.simulate_get(
        path="/convert",
        params={"amount": 0, "src_currency": "EUR", "dest_currency": "USD", "reference_date": "2019-12-31"},
    )
    assert result.json["error"].startswith("value must be at least 1.0 for dictionary value @ data['amount']")
    assert result.status == falcon.HTTP_400

    result = client.simulate_get(
        path="/convert",
        params={"amount": "1", "src_currency": "EUR", "dest_currency": "USD", "reference_date": "2019-00-31"},
    )
    assert result.json["error"].startswith("not a valid value for dictionary value @ data['reference_date']")
    assert result.status == falcon.HTTP_400

    result = client.simulate_get(
        path="/convert",
        params={"amount": "1", "src_currency": "EU", "dest_currency": "USD", "reference_date": "2019-00-31"},
    )

    assert result.json["error"].startswith(
        "length of value must be at least 3 for dictionary value @ data['src_currency']"
    )
    assert result.status == falcon.HTTP_400


def test_get_exchange_no_date(client: testing.TestClient, data: str) -> None:
    result = client.simulate_get(
        path="/convert",
        params={"amount": "1", "src_currency": "EUR", "dest_currency": "USD", "reference_date": "2019-12-31"},
    )
    assert result.json == {"error": "Date 2019-12-31 not found "}
    assert result.status == falcon.HTTP_400


def test_get_exchange_no_dest_currency(client: testing.TestClient, data: str) -> None:
    result = client.simulate_get(
        path="/convert",
        params={"amount": "1", "src_currency": "EUR", "dest_currency": "UUU", "reference_date": "2020-02-03"},
    )
    assert result.json == {"error": "Currency UUU not found"}
    assert result.status == falcon.HTTP_400


def test_get_exchange_ok(client: testing.TestClient, data: str) -> None:
    result = client.simulate_get(
        path="/convert",
        params={"amount": 1, "src_currency": "EUR", "dest_currency": "USD", "reference_date": "2020-02-03"},
    )
    assert result.json == {"amount": 1.1052, "currency": "USD"}
    assert result.status == falcon.HTTP_200

    result = client.simulate_get(
        path="/convert",
        params={"amount": 1, "src_currency": "USD", "dest_currency": "EUR", "reference_date": "2020-02-03"},
    )
    assert result.json == {"amount": 0.9048, "currency": "EUR"}
    assert result.status == falcon.HTTP_200
